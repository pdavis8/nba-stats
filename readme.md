Link to presentation: https://docs.google.com/presentation/d/1XffNBiQ_ltVkRiTKmvfp6CQfzUXKGVjPS81pzlvxcg8/edit?usp=sharing
# Usage
The server.py file and index.html files assume that the API is located at student10.cse.nd.edu on port 51053. To run the entire project, simply start the server with "python3 server.py" in the server directory, and then open the index.html file in a browser. The test files are located in the server directory and can be run with python3.
# JSON Specification
Request Type | Resource Endpoint | Body | Expected response | Inner working of handler
--- | --- | --- | --- | ---
GET | /stats/ | There should be no body to a GET request | string formatted json of all players’ stats | GET_INDEX: iterates through list of players and retrieves their stats
GET | /stats/:player | There should be no body to a GET request | string formatted json of one player's stats | GET: gets that player’s stats
GET | /stats/:team | There should be no body to a GET request | string formatted json of the stats of all the players on a team | GET_TEAM: gets the stats of players on that team
PUT | /stats/:player | ‘{"Team": "Boston Celtics", “PPG” : “27.0”, “RPG”: “7.4”, “APG”: “7.4”}’ | {“result”: “success”} if the operation worked | PUT: updates the PPG/RPG/APG/etc. stats of the player specified in the resource endpoint
POST | /stats/ | ‘{"Team": "Los Angeles Lakers", “name”: “Lebron James”, “data”: {“PPG” : “27.0”, “RPG”: “7.4”, “APG”: “7.4”}}’| {“result”: “success”} if the operation worked | POST creates a new player entry, with corresponding data as specified in the body
DELETE | /stats/:player | ‘{}’ | {“result”: “success”} if the operation worked | DELETE: deletes the specific player from the database
DELETE | /stats/ | ‘{}’ | {“result”: “success”} if the operation worked | DELETE_INDEX: deletes all players in the database
## OO API
load_team(): loads a team's stats from the given CSV file
load_all_players(): adds the stats of all players to the dictionary
# User interaction
When the user first loads the web client, a GET request will be sent to the server to retrieve data for all NBA players' stats sorted by points per game. The user can sort by different stats, search for a specific player, or filter only by a specific team.
# Complexity / Scale
The application is not very complex, as it was designed to be a simple interface which simplifies the process of comparing player stats between teams and in the entire league. The project does require handling large amounts of data.
