import csv
import re
class _players_database:
    def __init__(self):
        self.players = dict()
    
    def load_team(self, file_name, team_name):
        with open(file_name) as team_file:
            next(team_file)
            csv_reader = csv.reader(team_file, delimiter=',')
            for row in csv_reader:
                self.players[re.match('.*\\\\',row[1]).group()[:-1]] = {'Team': team_name, 'PPG': row[27], 'RPG': row[21], 'APG': row[22]}
    
    def load_all_players(self):
        self.load_team('stats/atlanta.csv', 'Atlanta Hawks')
        self.load_team('stats/boston.csv', 'Boston Celtics')
        self.load_team('stats/brooklyn.csv', 'Brooklyn Nets')
        self.load_team('stats/charlotte.csv', 'Charlotte Hornets')
        self.load_team('stats/chicago.csv', 'Chicago Bulls')
        self.load_team('stats/cleveland.csv', 'Cleveland Cavaliers')
        self.load_team('stats/clippers.csv', 'Los Angeles Clippers')
        self.load_team('stats/dallas.csv', 'Dallas Mavericks')
        self.load_team('stats/denver.csv', 'Denver Nuggets')
        self.load_team('stats/detroit.csv', 'Detroit Pistons')
        self.load_team('stats/goldenstate.csv', 'Golden State Warriors')
        self.load_team('stats/houston.csv', 'Houston Rockets')
        self.load_team('stats/indiana.csv', 'Indiana Pacers')
        self.load_team('stats/lakers.csv', 'Los Angeles Lakers')
        self.load_team('stats/memphis.csv', 'Memphis Grizzlies')
        self.load_team('stats/miami.csv', 'Miami Heat')
        self.load_team('stats/milwaukee.csv', 'Milwaukee Bucks')
        self.load_team('stats/minnesota.csv', 'Minnesota Timberwolves')
        self.load_team('stats/neworleans.csv', 'New Orleans Pelicans')
        self.load_team('stats/newyork.csv', 'New York Knicks')
        self.load_team('stats/okc.csv', 'Oklahoma City Thunder')
        self.load_team('stats/orlando.csv', 'Orlando Magic')
        self.load_team('stats/philadelphia.csv', 'Philadelphia 76ers')
        self.load_team('stats/phoenix.csv', 'Phoenix Suns')
        self.load_team('stats/portland.csv', 'Portland Trail Blazers')
        self.load_team('stats/sacramento.csv', 'Sacramento Kings')
        self.load_team('stats/sanantonio.csv', 'San Antonio Spurs')
        self.load_team('stats/toronto.csv', 'Toronto Raptors')
        self.load_team('stats/utah.csv', 'Utah Jazz')
        self.load_team('stats/washington.csv', 'Washington Wizards')

if __name__ == "__main__":
    players_db = _players_database()
    players_db.load_all_players()
    print(players_db.players)

