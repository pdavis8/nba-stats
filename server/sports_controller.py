import cherrypy                                                               
import re,json
import sys
sys.path.insert(0, '../ooapi')                                                       
from players_stats import _players_database
                                                                              
class SportsController(object):                                           
    def __init__(self):                                 
       # Prepopulating dictionary
        self.mydb = _players_database()
        self.mydb.load_all_players()
        self.myd = self.mydb.players

    def parse_name(self, name):
        new_name = ""
        for character in name:
            if character == '_':
                new_name += ' '
            else:
                new_name += character
        return new_name

    def GET(self, key):                                                       
        output = {'result': 'success'}                                        
        key = str(key)
        key = self.parse_name(key)                                                        
        try:                                                                  
            val = self.myd[key]                                               
            output['key'] = key;                                              
            output['value'] = val;                                            
        except KeyError as ex:                                                
            output['result'] = 'error'                                        
            output['message'] = 'key not found'                               
        return json.dumps(output)
    def GET_INDEX(self):                                                      
        output = {'result': 'success'}                                        
        output['entries'] = []                                                
        try:                                                                  
            for key, val in self.myd.items():                                 
                output['entries'].append({'name': key, 'data': val})          
        except Exception as ex:                                               
            output['result'] = 'error'                                        
            output['message'] = str(ex)                                       
        return json.dumps(output)                                             
    def GET_TEAM(self, key):
        output = {'result': 'success'}
        output['entries'] = []
        team = str(key)
        try:                                                                  
            for key, val in self.myd.items():                                 
                if team == val['Team']:
                    output['entries'].append({'name': key, 'data': val}) 
        except Exception as ex:                                               
            output['result'] = 'error'                                        
            output['message'] = str(ex)                                       
        return json.dumps(output)                                             
    def PUT(self, key):                                                       
        output = {'result': 'success'}                                        
        key = str(key)
        key = self.parse_name(key)                                                        
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))       
        try:                                                                  
            self.myd[key] = data                                 
        except Exception as ex:                                               
            output['result'] = 'error'                                        
            output['message'] = str(ex)                                       
        return json.dumps(output)                                             
    def POST(self):                                                           
        output = {'result': 'success'}                                        
        data = json.loads(cherrypy.request.body.read().decode('utf-8'))       
        try:
            n = self.parse_name(data['name'])                                                                  
            self.myd[n] = data['data']                             
        except Exception as ex:                                               
            output['result'] = 'error'                                        
            output['message'] = str(ex)                                       
        return json.dumps(output)                                             
    def DELETE_INDEX(self):                                                   
        self.myd.clear();                                                     
        return json.dumps({'result': 'success'})                              
    def DELETE(self, key):                                                    
        output = {'result': 'success'}                                        
        key = str(key)
        key = self.parse_name(key)     
        try:
            del self.myd[key]
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        return json.dumps(output)                                                   
