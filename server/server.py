import routes
import cherrypy
from sports_controller import SportsController

def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()
    sportsController = SportsController();
    dispatcher.connect('stats_get', '/stats/:key', controller = sportsController, action = 'GET', conditions = dict(method=['GET']))
    dispatcher.connect('stats_get_index', '/stats/', controller = sportsController, action = 'GET_INDEX', conditions = dict(method=['GET']))
    dispatcher.connect('stats_get_team', '/stats/team/:key', controller = sportsController, action = 'GET_TEAM', conditions = dict(method=['GET']))
    dispatcher.connect('stats_put', '/stats/:key', controller = sportsController, action = 'PUT', conditions = dict(method=['PUT']))
    dispatcher.connect('stats_post', '/stats/', controller = sportsController, action = 'POST', conditions = dict(method=['POST']))
    dispatcher.connect('stats_delete_index', '/stats/', controller = sportsController, action = 'DELETE_INDEX', conditions = dict(method=['DELETE']))
    dispatcher.connect('stats_delete', '/stats/:key', controller = sportsController, action = 'DELETE', conditions = dict(method=['DELETE']))
    
    dispatcher.connect('stats_options', '/stats/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('stats_key_options', '/stats/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('stats_team_options', '/stats/team/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    conf = {
        'global': {
            'server.socket_host': 'student10.cse.nd.edu',
            'server.socket_port': 51053
        },
        '/': {
            'request.dispatch': dispatcher,
            'tools.CORS.on': True
        }
    }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)
    
class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""
        
def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
