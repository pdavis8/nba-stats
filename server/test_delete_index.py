import unittest
import requests
import json

class TestCherrypyPrimer(unittest.TestCase):

        SITE_URL = 'http://student10.cse.nd.edu:51053' #'http://student04.cse.nd.edu:51053' #Replace this your port number and machine
        DICT_URL = SITE_URL + '/stats/'

        def reset_data(self):
                r = requests.delete(self.DICT_URL)

        def is_json(self, resp):
                try:
                        json.loads(resp)
                        return True
                except ValueError:
                        return False

        def test_sports_index_delete(self):
                self.reset_data()

                key1 = 'LebronJames'
                m1 = {}
                m1['value'] = {'PPG': 27, 'RPG': 7.4, 'APG': 7.4}
                r = requests.put(self.DICT_URL + key1, data = json.dumps(m1)) # uses put
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'success')

                key2 = 'ChrisPaul'
                m2 = {}
                m2['value'] = {'PPG': 18.3, 'RPG': 4.5, 'APG': 9.4}
                r = requests.put(self.DICT_URL + key2, data = json.dumps(m2)) # uses put
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'success')

                r = requests.delete(self.DICT_URL) # delete index
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'success')

                r = requests.get(self.DICT_URL + key1) # uses get
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'error')

                r = requests.get(self.DICT_URL + key2) # uses get
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'error')
                
if __name__ == "__main__":
        print('Testing for server: ' + TestCherrypyPrimer.SITE_URL)
        unittest.main()
