import unittest
import requests
import json

class TestCherrypyPrimer(unittest.TestCase):

        SITE_URL = 'http://student10.cse.nd.edu:51053' #'http://student04.cse.nd.edu:51053' #Replace this your port number and machine
        DICT_URL = SITE_URL + '/stats/'

        def reset_data(self):
                r = requests.delete(self.DICT_URL)

        def is_json(self, resp):
                try:
                        json.loads(resp)
                        return True
                except ValueError:
                        return False

        def test_sports_get_key(self):
                self.reset_data()
                key = 'LebronJames'
                r = requests.get(self.DICT_URL + key) # performing get
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'error')
                
if __name__ == "__main__":
        print('Testing for server: ' + TestCherrypyPrimer.SITE_URL)
        unittest.main()
