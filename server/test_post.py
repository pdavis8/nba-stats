import unittest
import requests
import json

class TestCherrypyPrimer(unittest.TestCase):

        SITE_URL = 'http://student10.cse.nd.edu:51053' #'http://student04.cse.nd.edu:51053' #Replace this your port number and machine
        DICT_URL = SITE_URL + '/stats/'

        def reset_data(self):
                r = requests.delete(self.DICT_URL)

        def is_json(self, resp):
                try:
                        json.loads(resp)
                        return True
                except ValueError:
                        return False

        def test_sports_index_post(self):
                self.reset_data()

                m = {}
                m['name'] = 'LebronJames'
                m['data'] = {'PPG': 27, 'RPG': 7.4, 'APG': 7.4}

                r = requests.post(self.DICT_URL, data = json.dumps(m)) # performing post
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'success')

                r = requests.get(self.DICT_URL) # uses get
                self.assertTrue(self.is_json(r.content.decode()))
                resp = json.loads(r.content.decode())
                self.assertEqual(resp['result'], 'success')

                
if __name__ == "__main__":
        print('Testing for server: ' + TestCherrypyPrimer.SITE_URL)
        unittest.main()
